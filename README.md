# covid19
Soporte de datos y visualización para el proyecto de mapeo


## Mapas Interactivos:


 *  [Centros Asistenciales Públicos de Salud](http://u.osmfr.org/m/437434/)

 *  [Farmacias](http://u.osmfr.org/m/435200/)

 *  [Servicios Esenciales](http://u.osmfr.org/m/437562/)


## Fuentes de datos:

 * OpenStreetMap [© OpenStreetMap contributors, Open Data Commons Open Database License (ODbL) by the OpenStreetMap Foundation (OSMF)](https://www.openstreetmap.org/copyright)*).

  * [consulta de hospitales de Misiones con overpass-turbo](https://overpass-turbo.eu/s/S2v).
  
  * [consulta de farmacias de Misiones con overpass-turbo](https://overpass-turbo.eu/s/S2x).

  * [consulta de CAPS en Misiones con overpass-turbo](https://overpass-turbo.eu/s/S2w).

  * [consulta de mercados en Misiones con overpass-turbo](https://overpass-turbo.eu/s/S2A).

  * [consulta de ferreterías en Misiones con overpass-turbo](https://overpass-turbo.eu/s/S2B).

  * [consulta de verdulerías en Misiones con overpass-turbo](https://overpass-turbo.eu/s/S2C).

  * [consulta de panaderías en Misiones con overpass-turbo](https://overpass-turbo.eu/s/S38).

  * [consulta de estaciones de servicio en Misiones con overpass-turbo](https://overpass-turbo.eu/s/S2R).
  
  * [consulta de ferias francas en Misiones con overpass-turbo](https://overpass-turbo.eu/s/S7t).
  * 
  * [consulta de centros de cobro en Misiones con overpass-turbo](https://overpass-turbo.eu/s/SbO).

